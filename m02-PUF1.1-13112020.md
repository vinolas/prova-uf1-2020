# M02 UF1.1

1J/H ISM GRUP A - Divendres 13 Nov 2020

## Entrega

**I M P O R T A N T**

- **LA PRIMERA PART DE CONVERSIONS DE BASE S'HA DE FER EN PAPER**
- **LA RESTA DE PREGUNTES S'HAN D'ENTREGAR EN FITXER DE TEXT FORMAT MARKDOWN I EXTENSIÓ .md**

## Enunciat

## Conversions de base [4 PUNTS]

1. Converteix els següents codis a les altres bases: 

**ENTREGAR TAMBÉ ELS CÀLCULS**

| DECIMAL | BINARI          | HEXADECIMAL | OCTAL |
| ------- | --------------- | ----------- | ----- |
| 20      |                 |             |       |
|         | 1 0 1 0 1 0 0 1 |             |       |
|         |                 | B1          |       |
|         |                 |             | 127   |
| 116     |                 |             |       |
|         | 1 0 0 1 0 0 0 1 |             |       |
|         |                 | 7           |       |
|         |                 |             | 4     |
| 254     |                 |             |       |
|         |             1 1 |             |       |
|         |                 | EF          |       |
|         |                 |             | 8     |
| 32      |                 |             |       |
|         | 1 1 1 1 1 1 1 1 |             |       |
|         |                 | A6          |       |
|         |                 |             | 27    |

## Conceptes de Sistemes Operatius [4 PUNTS]

1. **Com gestionarà el sistema operatiu la recepció de dades cap a múltiples aplicacions simultàniament per una única interfície, per exemple de xarxa?**  [0,5 punts]

- Resposta: 

2. **De quins mecanismes disposem per a enviar les dades de múltiples aplicacions (processos) per un dispositiu de l'ordinador i evitar així que hi hagi una colisió?**  [0,25 punts]

- Resposta: 

3. **A partir de la següent sortida d'una ordre, responeu les següents preguntes:** [1,5 punts]

```
  1  [      0.0%]    4  [******100.0%]   7  [     0.0%]
  2  [      0.0%]    5  [        0.0%]   8  [**** 0.0%]
  3  [**    0.0%]    6  [        0.0%]
  Mem[||||||||#*****************************  1.76G/12.8G]   Tasks: 45, 175 thr; 1 running
  Swp[                                           0K/0.0G]   Load average: 0.30 0.25 0.34
                                                             Uptime: 24 days, 02:10:00
    PID USER      PRI  NI  VIRT   RES   SHR S CPU% MEM%   TIME+  Command
      1 root       20   0  167M 12824  8536 S  0.0  0.0  1:30.09 /sbin/init
    415 root20   0  327M  234M  233M S  0.0  0.4  2:23.71 /lib/systemd/systemd-journald
    432 root20   0 21132  5288  4172 S  0.0  0.0  0:01.73 /lib/systemd/systemd-udevd

```

​		A) Quina ordre ens mostrarà aquesta informació?

​				Resposta: 

​		B) Com podem instal.lar aquest aplicatiu de CLI en un sistema operatiu Fedora?

​				Resposta: 

​		C) Quants processadors veu el sistema operatiu?

​				Resposta: 

​		D) Quanta memòria total gestiona el sistema operatiu?

​				Resposta: 

​		E) Quanta memòria SWAP està utilitzant actualment el sistema operatiu?

​				Resposta: 

​		E) Quin és el número de procés per a *systemd-journald*?

​				Resposta:

4. **Respecte a la memòria 'swap', contesta:**  [0,25 punts]
    $ free -m
             total        used        free      shared  buff/cache   available
    Mem:           3746        1337         822         263        1587        1819
    Swap:          3875           0        3875

- En quin dispositiu físic de l'ordinador s'emmagatzema la memòria 'swap' en un...?:
  - En un sistema operatiu Linux: 
  - En un sistema operatiu Windows:  


5. **Explica les diferències entre les BBDD documentals i les no documentals:**  [0,75 punts]




6. **Indica tres fonts d'informació de transaccions:** [0,75 punts]

- Resposta 1: 
- Resposta 2: 
- Resposta 3: 




## Ordres [2 PUNTS]

1. **Amb quina ordre puc veure els dispositius d'emmagatzematge de blocks que ha detectat el sistema operatiu?** [0,5 punts]

- Resposta (mostra també la sortida de l'ordre al teu ordinador): 

- Quantes particions té el dispositiu sda (o vda si és una màquina virtual) en el teu cas?: 

2. **Com podem veure la velocitat actual del processador al sistema operatiu Linux?** [0,5 punts]

- Resposta (Mostreu la sortida fins on es vegi, al menys, la velocitat actual del processador):


3. **Quina ordre ens permet canviar els permisos del fitxer mostrat per tal que només el grup d'usuaris a qui pertany pugui executar-lo?** Nota: Els permisos actuals no s'han de modificar  [0,5 punts]
3826509     4 -r-x---r--.  1 darta darta      874  2 oct 16:19 install.sh

- Resposta: 


4. **Sobre propietaris i permisos de fitxers respon:**  [0,5 punts]

   Quins permisos tindrà el grup d'usuaris sobre un fitxer al que apliquem els permisos de fitxer 755?

- Resposta: 

  Com podem canviar el propietari del fitxer install.sh anterior per tal que ara pertanyi al grup d'usuaris 'manolitos'?

- Resposta: 





